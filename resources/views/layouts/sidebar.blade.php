<div class="main-sidebar">
  <aside id="sidebar-wrapper">
    <div class="sidebar-brand mb-5">
      <img src="../assets/img/logopd/logo_pd4.png" alt="logo" width="170">
    </div>
    <div class="sidebar-brand sidebar-brand-sm mt-3">
      <a href="/"><img src="../assets/img/logopd/logo_pd4.png" alt="Peduli Diri" width="50"></a>
    </div>
    <ul class="sidebar-menu">
        <li class="menu-header">Dashboard</li>
        <li class="nav-item dropdown">
          <a href="#" class="nav-link has-dropdown"><i class="fas fa-th-large"></i><span>Menu</span></a>
          <ul class="dropdown-menu">
            <li><a class="nav-link" href="/dashboard">Dashboard</a></li>
            <li><a class="nav-link" href="/inputperjalanan">Input Data Perjalanan</a></li>
            <li><a class="nav-link" href="/dataperjalanan">Data Perjalanan</a></li>
          
          </ul>
       
  </aside>
</div>